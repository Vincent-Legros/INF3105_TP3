OPTIONS=-O2 -Wall

all : tp3 chemin

tp3 : tp3.cpp pointst.o bixi.o carte.o
	g++ ${OPTIONS} -o tp3 tp3.cpp pointst.o bixi.o carte.o

chemin : chemin.cpp carte.h carte.o pointst.h pointst.o
	g++ ${OPTIONS} -o chemin chemin.cpp pointst.o carte.o
	
pointst.o : pointst.cpp pointst.h
	g++ ${OPTIONS} -c -o pointst.o pointst.cpp

bixi.o : bixi.cpp bixi.h pointst.h
	g++ ${OPTIONS} -c -o bixi.o bixi.cpp

carte.o : carte.cpp carte.h pointst.h
	g++ ${OPTIONS} -c -o carte.o carte.cpp

clean:
	rm -rf tp3 chemin *~ *.o

