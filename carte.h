/*  INF3105 | Structures de données et algorithmes (Automne 2017)
    UQAM | Département d'informatique
    TP3 | http://ericbeaudry.uqam.ca/INF3105/tp3/
    
    Fichier de départ carte.h © Éric Beaudry.
 
    Modifié par:
     (1) Nom + Code permanent du l'étudiant.e 1
     (2) Nom + Code permanent du l'étudiant.e 2
*/

#if !defined(_CARTE__H_)
#define _CARTE__H_

#include <istream>
#include <string>
#include <list>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <assert.h>
#include "pointst.h"

using namespace std;

// La classe carte n'a pas besoin d'être une classe modèle (template).
class Carte{
  public:
    // Fonctions pour ajouter les lieux et les routes dans la cartes. Elles sont appelées par l'opérateur >>.
    void ajouterLieu(const string& nom, const PointST& p);
    void ajouterRoute(const string& nom, list<string>& noms);
    
    // Retourne le nom du lieux le plus près du point p.
    const string& getNoeudPlusPres(const PointST& p) const;

    // Fonction pour tester le bon fonctionnement de l'algorithme de recherche de chemins (Dijkstra ou Floyd-Warshall)
    // Reçoit deux noms de lieux. Le chemin est retourné dans la liste out_cheminnoeuds passée en référence.
    // Retourne la distance.
    double calculerChemin(const string& origine, const string& destination, list<string>& out_cheminnoeuds) const ;

    // Fonction qui calcule un chemin entre deux coordonnées. Appelé dans le main() dans tp3.cpp.
    double calculerChemin(const PointST& origine, const PointST& destination, list<string>& out_cheminnoeuds) const ;

  private:
    class Sommet ; 
    struct Arete {
        double distance  ; 
        Sommet* destination ;
        //string  destination ; 
        
        Arete(double poids, Sommet& a): distance(poids), destination(&a){};         
    
        bool operator < (const Arete& a) const { return distance < a.distance ; } 
    
    };
    struct Sommet{
        
        string  nom ; 
        PointST coord;
        Sommet():coord(){}; 
        Sommet(const string& n , const PointST& pt):nom(n),coord(pt){} 
        set<Arete> voisins ;  
    };
    //vector<Sommet> sommets ; 
    map<string,Sommet> sommets ; 
    
    // À compléter.
    // Inspirez-vous de la classe Graphe vue en classe ou celle des labs 11 et 12.
   
    
  friend istream& operator >> (istream& is, Carte& carte);
};

#endif

