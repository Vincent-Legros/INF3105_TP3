/*  INF3105 | Structures de données et algorithmes (Automne 2017)
    UQAM | Département d'informatique
    TP3 | http://ericbeaudry.uqam.ca/INF3105/tp3/
    
    Fichier de départ tp3.cpp © Éric Beaudry.

    Ce fichier contient une solution simple du TP1. Elle a été adaptée au TP3.
    
    À moins de l'avoir modifié, n'imprimez pas ce fichier.
*/

#include <fstream>
#include <iostream>
#include <string>
#include "bixi.h"
#include "carte.h"

using namespace std;

void tp3(ReseauBixi& reseau, Carte& carte, istream& isrequetes){
    while(isrequetes){
        PointST p1, p2;
        isrequetes >> p1 >> p2;
        //string plusproche1 = carte.getNoeudPlusPres(p1);
        //string plusproche2 = carte.getNoeudPlusPres(p2);
        if(!isrequetes) break;
        reseau.servir(p1, p2, carte);
    }
}

int main(int argc, const char** argv)
{
    if(argc<3){
        cout << "Syntaxe: ./tp3 reseau.txt carte.txt [requetes.txt]" << endl;
        return 1;
    }
    
    ReseauBixi reseau;
    Carte carte;
   
    ifstream fichierreseau(argv[1]);
    if(fichierreseau.fail()){
        cout << "Erreur ouverture : " << argv[1] << endl;
        return 1;
    }
    fichierreseau >> reseau;
    cerr << "fin du chargement de réseau" << endl ;  
    ifstream fichiercarte(argv[2]);
    if(fichiercarte.fail()){
        cout << "Erreur ouverture : " << argv[2] << endl;
        return 1;
    }
    fichiercarte >> carte;
    cerr << "fin du chargement de la carte" << endl ; 
    if(argc==4){
        ifstream fichierrequetes(argv[3]);
        if(fichierrequetes.fail()){
            cout << "Erreur ouverture : " << argv[3] << endl;
            return 1;
        }
        tp3(reseau, carte, fichierrequetes);
    }else
        tp3(reseau, carte, cin);
    
    return 0;
}

