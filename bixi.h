/*  INF3105 | Structures de données et algorithmes (Automne 2017)
    UQAM | Département d'informatique
    TP3 | http://ericbeaudry.uqam.ca/INF3105/tp3/
    
    Fichier de départ bixi.h © Éric Beaudry.
 
    Modifié par:
     (1) Nom + Code permanent du l'étudiant.e 1
     (2) Nom + Code permanent du l'étudiant.e 2
*/

#if !defined(_BIXI__H_)
#define _BIXI__H_

#include <iostream>
#include <vector>
#include "pointst.h"
#include "carte.h"

using namespace std;

class Station{
  public:
    Station();
    const PointST& getPosition()const;
    const string& getNom()const;
    bool aVeloDisponible()const;
    bool aPADDisponible()const;
    
    void rendreVelo();
    void prendreVelo();

  private:
    int id;
    PointST position;
    string nom;
    int nbVelos; // nombre de vélos disponibles
    int nbPAD; // nombre de points d'ancrage disponibles
    
  friend istream& operator >> (istream& is, Station& station);
  friend ostream& operator << (ostream& is, const Station& station);
};

class ReseauBixi{
  public:
    
    void servir(const PointST& origine, const PointST& destination, const Carte& carte);

  private:
    vector<Station> stations;

  friend istream& operator >> (istream& is, ReseauBixi& reseau);
  friend ostream& operator << (ostream& is, const ReseauBixi& reseau);
};

#endif
