/*  INF3105 | Structures de données et algorithmes (Automne 2017)
    UQAM | Département d'informatique
    TP3 | http://ericbeaudry.uqam.ca/INF3105/tp3/
    
    Fichier de départ carte.cpp © Éric Beaudry.
 
    Modifié par:
        Vincent Legros LEGV22078300 
*/

#include "carte.h"
#include <limits>

void Carte::ajouterLieu(const string& nom, const PointST& p){
    /* Étape 1.
       Inspirez-vous de la fonction Graphe<A>::ajouterSommet dans graphe.h de Lab11 et Lab12.
    */
    Sommet lieu = Sommet(nom,p); 
    sommets[nom] = lieu ; 
}

void Carte::ajouterRoute(const string& nom, list<string>& route){
    /* Étape 3.
      
      Inspirez-vous de la fonction Graphe<A>::ajouterAreteOrientee dans graphe.h de Lab11 et Lab12.
      Ce graphe doit être orienté, car on considère les sens uniques (on ne peut circuler à contre-sens à pied et en vélo).
      Ajoutez une arête pour chaque paire de noms de lieu consécutif dans route.
      Si la liste route contient <a, b, c, ..., z>, alors il faut ajouter les arêtes : (a,b), (b,c), ..., (y,z).
    */
    list<string>::const_iterator iter =  route.cbegin(); 
    while(iter != route.end()){
        //cerr << "debut du while " << endl ; 
        string  depart = *iter ; 
        //cerr << "depart = " <<  depart <<endl ; 
        iter++ ; 
        if (iter == route.end() ) break ; 
        string  destination = *iter ; 
        Sommet &sDepart = sommets[depart]; 
        Sommet &sArrive = sommets[destination];  
        double poids = sDepart.coord.distance(sArrive.coord); 
        //cerr << depart << "-->" <<  destination << " " << poids << endl ; 
        sDepart.voisins.insert(Arete(poids,sArrive));  
        //cerr << "Allocation réussi" << endl ; 
    }
    //cerr << "sortie du while" <<endl ; 
}

string rien = "?";
const string& Carte::getNoeudPlusPres(const PointST& p) const
{
    // Étape 2.
    // Il faut itérer sur tous les lieux et retourner celui ayant la plus petites distance avec c.
    string &plusPres = rien  ; 
    double distance_min  = numeric_limits<double>::infinity(); 
    double dist ; 
    for ( pair<string,Sommet>  s  : sommets ) {
        dist = s.second.coord.distance(p); 
        if (distance_min > dist){
            distance_min = dist ;  
            plusPres = s.first ; 
        }
    }
    return plusPres;
}

// La fonction la plus importante du TP3.
// Calcule le chemin le plus court entre les lieux nomorigine et nomdestination.
// Le chemin est retourné dans la liste out_cheminnoeuds passée en référence.
// Retourne la distance.
double Carte::calculerChemin(const string& nomorigine, const string& nomdestination, list<string>& out_cheminnoeuds) const 
{
    /* Étape 4.
        
    Conseils pour l'implémentation de Dijkstra :
    
    - Utilisez un std::priority_queue.
    - À noter qu'un priority_queue trie les éléments en ordre décroissant. L'opérateur < doit donc faire une comparaison > sur la distance.
    - À chaque itération :
     -- affichez les noeuds visités avec leur distance.
     -- Le premier devrait être le noeud d'origine avec 0.
     -- L'ordre de distances doit être croissant.
     -- Pour chaque noeud connecté au noeud visité, affichez sa distance.
    - Une fois la boucle while terminée, le chemin peut être construit en partant du noeud de destination jusqu'à origine en utilisant le parent.
    - Si aucun chemin n'existe, la fonction devrait retourner +infini.
    */
    priority_queue<pair<double,string> >  q; 
    q.push(pair<double,string>(0,nomorigine)); 

    //map<string,double>dist(sommets.size(),numeric_limits<double>::infinity()) ; 
    //dist[0] =  0 ; 
    //vetor<int>distances(sommets.size(),numeric_limits<double>::infinity()); 
    map<string,double> distances ; 
    for (pair<string,Sommet> s : sommets){
        distances[s.first] =  numeric_limits<double>::infinity(); 
    }

    map<string,pair<string,double> > resultat ; 
    resultat[nomorigine].second = 0 ; 
    
    distances[nomorigine] = 0 ; 
    //cheminnoeud.push(); 
    while(!q.empty()){
        string u = q.top().second;
        q.pop();  
        //cerr << u  << " " << distances[u]  << endl ; 
        const  Sommet som  =  sommets.at(u); 
        const set<Arete> &vo = som.voisins ; 
        //cerr << "nombre de voisins" << vo.size() << endl ; 
        for (const  Arete& arete : vo ) {
            double  poids = arete.distance  ; 
            Sommet& v = *arete.destination ; 
            if (poids + distances[u] < distances[v.nom] ) {
               distances[v.nom] = poids + distances[u];  
               q.push(pair<double,string>(-distances[v.nom],v.nom));  
               resultat[v.nom] = pair<string,double>(u,distances[v.nom]); 
            }
        }   
    }   
    //cerr << "fin des calcule de la table de correspondance "<<endl ; 
    string prochain = nomdestination ; 
    pair<string,double> n; 
    do {
        n = resultat[prochain];
        out_cheminnoeuds.push_front(prochain); 
        prochain = n.first ; 
        //cerr << "prochain :" <<  prochain << ":"<< endl ; 
    } while (n.second != 0 );
    //} while (n.first != nomorigine &&  !n.first.empty()) ;
    //out_cheminnoeuds.push_front(nomorigine);
    //cerr << "fin de la création du chemin " <<endl ; 
    
    return distances[nomdestination]; 

}


double Carte::calculerChemin(const PointST& origine, const PointST& destination, list<string>& out_cheminnoeuds) const 
{
    string noeudorigine = getNoeudPlusPres(origine);
    string noeuddestination = getNoeudPlusPres(destination);
    double d = calculerChemin(noeudorigine, noeuddestination, out_cheminnoeuds);
    // Étape 5 :
    PointST ptO  = sommets.at(noeudorigine).coord;
    PointST ptD  = sommets.at(noeuddestination).coord;    
    d+=  ptO.distance(origine);
    d+=  ptD.distance(destination);
    return d;
}



/* Lire une carte. */
istream& operator >> (istream& is, Carte& carte)
{
    // Lire les lieux
    while(is){
        string nomlieu;
        is >> nomlieu;
        if(nomlieu == "---") break;
        PointST p;
        char pv;
        is >> p >> pv;
        assert(pv==';');
        carte.ajouterLieu(nomlieu, p);
    }

    // Lire les routes
    while(is){
        string nomroute;
        is >> nomroute;
        if(nomroute == "---" || nomroute=="" || !is) break;
        
        char deuxpoints;
        is >> deuxpoints;
        assert(deuxpoints == ':');
        
        std::list<std::string> listenomslieux;

        string nomlieu;        
        while(is){
            is>>nomlieu;
            if(nomlieu==";") break;
            assert(nomlieu!=":");
            assert(nomlieu.find(";")==string::npos);
            listenomslieux.push_back(nomlieu);
        }
        
        assert(nomlieu==";");
        ////cerr << "debut " << nomroute << endl ;  
        carte.ajouterRoute(nomroute, listenomslieux);
        //cerr << "route " << nomroute << " ajouté" << endl ; 
    }
    
    return is;
}

